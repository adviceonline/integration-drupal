<?php
namespace Drupal\ChatPirate;
/**
 * @file
 * Chatpirateapi.class.php.
 */

/**
 * ChatPirate API Class.
 */
class ChatPirateApi {
  public  $serverUrl;
  private $authLogin;
  private $authPassword;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->serverUrl = 'https://app.chatpirate.com/';
  }

  /**
   * Auth Set.
   *
   * @var $auth_login
   * @var $auth_password
   */
  public function authSet($auth_login, $auth_password) {

    $this->authLogin = $auth_login;
    $this->authPassword = $auth_password;
  }

  /**
   * Login by Token.
   *
   * @var $email
   * @var $token
   * @var string
   *
   * @return string
   *   Login url.
   */
  public function cpLoginByToken($email, $token) {

    $url = 'api/v1/register/loginByToken/' . $email . '/' . $token . '?redirect=true';

    return $this->serverUrl . '' . $url;
  }

  /**
   * Return server url.
   *
   * @return string
   *   Server url.
   */
  public function cpLoginUrl() {

    return $this->serverUrl;
  }


  /**
   * Login by Password.
   *
   * @return mixed|bool
   *   Company informations.
   */
  public function cpLoginByPassword() {

    $url = 'api/v1/register/getToken';

    $headers = array(
      "Authorization: " . $this->authLogin . "::" . $this->authPassword,
    );

    return $this->curl($url, NULL, $headers);
  }

  /**
   * Check Auth.
   *
   * @return bool
   *   TRUE If logged.
   */
  private function authCheck() {

    if (isset($this->authLogin) && !empty($this->authLogin)
      && isset($this->authPassword) && !empty($this->authPassword)
    ) {
      return TRUE;
    }
    else {
      return FALSE;
    }

  }

  /**
   * Is cURL php extension enabled.
   *
   * @return bool
   *   TRUE or FALSE
   */
  private function isCurl() {
    if (in_array('curl', get_loaded_extensions())) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * API requests.
   *
   * @var $url
   * @var null $data
   * @var null $additional_headers
   * @var bool $skipAuth
   * @var bool|mixed
   *
   * @return mixed|bool
   *    API Response.
   */
  private function curl($url, $data = NULL, $additional_headers = NULL, $skip_auth = FALSE) {

    if ($this->authCheck() || $skip_auth == TRUE) {
      if ($this->isCurl()) {
        $api_url = $this->serverUrl . '' . $url;

        $json_data_encoded = json_encode($data);

        $ch = curl_init($api_url);

        $header = array(
          "Content-Type: application/json",
        );

        if (!empty($additional_headers)) {
          $header = array_merge($header, $additional_headers);
        }

        // POST.
        if (!empty($data)) {

          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data_encoded);
        }
        else {
          // GET.
        }

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        if (!$skip_auth) {
          curl_setopt($ch, CURLOPT_USERPWD, $this->authLogin . ":" . $this->authPassword);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); /* won't fix */
        $c_result = curl_exec($ch);
        $result = json_decode($c_result);

        if ($result === FALSE) {
          return FALSE;
        }
        elseif (isset($result->error)) {
          return $result;
        }
        else {
          return $result;
        }
      }
      else {
        $result['error']['message'] = "ChatPirate requires the PHP <a href='http://php.net/manual/en/curl.setup.php'>cURL</a> library.";
        return json_decode(json_encode($result));
      }
    }
    else {
      return FALSE;
    }
  }

}
