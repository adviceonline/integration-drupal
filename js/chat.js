/**
 * @file
 * Chat.js.
 */

(function () {

  'use strict';

  window.__cp = {};
  window.__cp.license = drupalSettings.bar.message;

  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
  s.src = (window.location.protocol === 'https:' ? 'https://' : 'http://') + 'cdn.chatpirate.com/plugin.js';
  var sc = document.getElementsByTagName('script')[0]; sc.parentNode.insertBefore(s, sc);

}());
