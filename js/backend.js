/**
 * @file
 * Backend.js.
 */

(function ($) {

  'use strict';

  $('#logout a').click(function () {
    $('#logout-form').fadeIn('slow');
    $('#create_account_after_logout').fadeIn('slow');
  });

}(jQuery));
