<?php
namespace Drupal\ChatPirate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ChatPirate\ChatPirateApi as Api;

/**
 * @file
 * ChatPirateController.php.
 */

/**
 * ChatPirate Database Class.
 */
class DrupalDb {
  private static $instance;

  /**
   * Constructor.
   */
  private function __construct() {}

  /**
   * Clone.
   */
  private function __clone() {}

  /**
   * Singletone Design Pattern.
   *
   * @return \Drupal\ChatPirate\Controller\DrupalDb
   *   DrupalDb instance.
   */
  public static function getInstance() {
    if (self::$instance === NULL) {
      self::$instance = new DrupalDb();
    }
    return self::$instance;
  }

  /**
   * Save variables to database.
   *
   * @var $key
   * @var $val
   */
  public function variableSet($key, $val) {
    $save = \Drupal::configFactory()->getEditable('system.site');
    $save->set($key, $val);
    $save->save();
  }

  /**
   * Selects variables from database.
   *
   * @var $key
   * @var $val
   */
  public function variableGet($val) {
    $system = \Drupal::config('system.site');
    $ret = $system->get($val);

    return $ret;
  }

}

/**
 * Implements an chatpirate controller.
 */
class ChatPirateController extends ControllerBase {

  public $db;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->db = DrupalDb::getInstance();
  }

  /**
   * Checks if the account is connected.
   *
   * @return mixed
   *   Chatpirate Form.
   */
  public function chatpirate() {

    if ($this->db->variableGet('chatpirate_companyid') &&
        $this->db->variableGet('chatpirate_token') &&
        $this->db->variableGet('chatpirate_email')) {
      return \Drupal::formBuilder()->getForm(new ChatPirateFormAfter());
    }
    else {
      return \Drupal::formBuilder()->getForm(new ChatPirateForm());
    }
  }

}

/**
 * Class ChatPirateForm.
 *
 * @package Drupal\ChatPirate\Controller
 */
class ChatPirateForm extends FormBase {

  public $db;
  public $api;
  public $errors;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->db = DrupalDb::getInstance();

    require drupal_get_path('module', 'chatpirate') . '/chatpirateapi.class.php';
    $this->api = new Api();
  }

  /**
   * Get Form Id.
   *
   * @return string
   *   Returns Chatpirate Form id.
   */
  public function getFormId() {
    return 'chatpirate_form';
  }

  /**
   * Provides form elements.
   *
   * @return array
   *   Form prefix & suffix.
   */
  public function formBody() {
    $form = array(
      '#prefix' => '<div id="chatpirate-logo"></div><span class="chatpirate-header-text">' . t('Connect Drupal with Your ChatPirate Account') . '</span>
                          <div id="login-form" class="chatpirate-form">',
      '#suffix' => '</div><span id="create_account">Haven\'t an account yet? <a target="_blank" href="//chatpirate.com/signup/createdType/drupal">Sign up</a></span>',
    );

    return $form;
  }

  /**
   * Form builder.
   *
   * @var array $form
   * @var \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = $this->formBody();

    $form['#attached']['library'][] = 'chatpirate/backend';

    $form['email'] = array(
      '#type'  => 'textfield',
      '#title' => t('E-mail'),
      '#required' => TRUE,
    );
    $form['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    return $form;
  }

  /**
   * Form validator.
   *
   * @var array $form
   * @var \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
      $this->errors = TRUE;
      $form_state->setErrorByName('email', $this->t('Given address email is incorrect. Valid email address should be in the format local-part@hostname'));
    }
  }

  /**
   * Form submit.
   *
   * @var array $form
   * @var \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   *   Notification message.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $email = $form_state->getValue('email');
    $pass  = $form_state->getValue('password');

    $this->api->authSet($email, $pass);
    $login_by_password = $this->api->cpLoginByPassword();

    if (isset($login_by_password->error->message)) {
      $this->errors = TRUE;
      drupal_set_message($this->t('Error:') . $login_by_password->error->message, 'error');
    }
    elseif ($login_by_password == FALSE) {
      $this->errors = TRUE;
      drupal_set_message($this->t('Error: Something went wrong.'), 'error');
    }
    else {
      if (isset($login_by_password->data->companyId) && !empty($login_by_password->data->companyId) &&
            isset($login_by_password->data->token) && !empty($login_by_password->data->token) &&
            isset($login_by_password->data->email) && !empty($login_by_password->data->email)) {

        $companyid = $login_by_password->data->companyId;
        $token = $login_by_password->data->token;
        $email = $login_by_password->data->email;

        $this->db->variableSet('chatpirate_companyid', $companyid);
        $this->db->variableSet('chatpirate_token', $token);
        $this->db->variableSet('chatpirate_email', $email);

        return drupal_set_message(t('You have successfully connected your ChatPirate account!'), 'status');

      }
      else {
        $this->errors = TRUE;
        drupal_set_message($this->t('Error: Something went wrong.'), 'error');
      }
    }
  }

}

/**
 * Implements an chatpirate form view after connecting with chatpirate.
 */
class ChatPirateFormAfter extends ChatPirateForm {

  /**
   * Provides form elements.
   *
   * @return array
   *   Form prefix & suffix.
   */
  public function formBody() {

    $token = $this->db->variableGet('chatpirate_token');
    $email = $this->db->variableGet('chatpirate_email');

    $go_to_app_url = $this->api->cpLoginByToken($email, $token);

    $form = array(
      '#prefix' => '<div id="chatpirate-logo"></div>
                          <span class="gototext">' . $this->t("In order to launch ChatPirate, click [Go to App].") . '</span>
                          <a target="_blank" class="btn_chatpirate_login" href="' . $go_to_app_url . '">' . $this->t("Go to App") . '</a>
                          <span id="logout">' . $this->t("Sign in to a different account?") . ' <a href="#">' . $this->t("Log out") . '</a></span>
                          <div id="logout-form" class="chatpirate-form">',
      '#suffix' => '</div><span id="create_account_after_logout">' . $this->t("Create new account") . ' <a target="_blank" href="//chatpirate.com/signup/createdType/drupal">' . $this->t("Sign up") . '</a></span>',
    );

    return $form;
  }

  /**
   * Destructor.
   */
  public function __destruct() {
    if (!isset($this->errors)) {
      drupal_set_message(t('You have successfully connected your ChatPirate account!'), 'status');
    }
  }

}
